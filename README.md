# Rust Actix Web Service
![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-4/badges/main/pipeline.svg)

This project is a simple web service built with Rust and the Actix web framework. It demonstrates how to containerize a Rust Actix application using Docker for easy deployment and scaling.

## Prerequisites

Before you begin, ensure you have the following installed on your system:

- Docker
- Rust
- Cargo

## Local Development

To develop and test the application locally, follow these steps:

1. Clone the repository:

   ```
   git clone <repository-url>
   ```

2. Navigate to the project directory:

   ```
   cd actix_web_app
   ```

3. Build and run the application:

   ```
   cargo run
   ```

The web service should now be running and accessible at `http://localhost:8080`.

## Containerization with Docker

To build and run the application using Docker, follow these steps:

### Building the Docker Image

1. From the project root directory, build the Docker image:

   ```
   docker build -t actix_web_app .
   ```

This command builds a Docker image named `actix_web_app` based on the instructions in the `Dockerfile`.
![build](./pic/build.png)

### Running the Docker Container

2. Run the application inside a Docker container:

   ```
   docker run --name my_actix_app -p 8080:8080 -d actix_web_app
   ```

This command runs the `actix_web_app` image in a container named `my_actix_app`. It maps port 8080 inside the container to port 8080 on your host machine, allowing you to access the web service at `http://localhost:8080`.
![run](./pic/run.png)

## Accessing the Application

With the application running, you can access it through your web browser or using a tool like `curl`:

```
curl http://localhost:8080
```

Replace the URL path with the appropriate endpoints defined in your application.
![web](./pic/web.png)

## Stopping and Removing the Container

To stop the running container:

```
docker stop my_actix_app
```

To remove the stopped container:

```
docker rm my_actix_app
```